#include <atmel_start.h>
#include "avr/delay.h"
#include "glcd/glcd.h"
#include "glcd/fonts/Font5x7.h"
#include "glcd/fonts/Liberation_Sans15x21_Numbers.h"
#include "24lc1025_twi.h"

float test_float = 1.5;

/**
 * [test_rw_eeprom --> Test EEPROM]
 */
int test_rw_eeprom(void){

    #define DEBUG_TEST_EEPROM
    
    // after addr wdg counter
    #define ADDR_TEST 60003      
    #define LEN_TEST 3

    unsigned long a; 
    int b;
    int status;                
    unsigned char write_value[LEN_TEST];
    unsigned char read_value[LEN_TEST];

    // test write
    b =0;
    for (a=ADDR_TEST;a<ADDR_TEST+LEN_TEST;a++)
    {
        write_value[b] = b;
//        printf("write ee\n\r");
        EEWrite(a,(uint8_t *)&write_value[b],1);
//        printf("write ee done\n\r");
        b++;
    }
    // test read
    b =0;
    for (a=ADDR_TEST;a<ADDR_TEST+LEN_TEST;a++)
    {                        
//        printf("read ee\n\r");
         EERead(a,(uint8_t *)&read_value[b],1) ;
//        printf("read ee done\n\r");
         b++;         
    }     
    // compare value
    for (b=0;b<LEN_TEST;b++)
    {
        if(read_value[b] != write_value[b]){
            #ifdef DEBUG_TEST_EEPROM
                printf("read[%d]=0x%x not equal write[%d]=0x%x\n\r",b,read_value[b],b,write_value[b]);
            #endif
            status = -1;
        }               
        else{
            #ifdef DEBUG_TEST_EEPROM
                printf("read[%d]=0x%x equal write[%d]=0x%x\n\r",b,read_value[b],b,write_value[b]);
            #endif
            status = 0;
        }
    }     
    return status;            
}

int main(void)
{
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();
	
	/* Enable interrupts */
	//sei();
	
	printf("PT.Korosi Specindo\r\n");
	printf("RnD Team\r\n");
	printf("FW DCI\r\n");
	printf("v2.1.0\r\n");
	
	glcd_init();

	#if 1
		//stdin=stdout=&amp;uart0_str;
		char string[8] = "";
		double aa= 2.456/1000;
		
		glcd_clear_buffer();

		glcd_set_font(Liberation_Sans15x21_Numbers,15,21,46,57);
		sprintf(string,"%.3f",0.256);
		glcd_draw_string_xy(0,20,string);
		glcd_write();

		glcd_tiny_set_font(Font5x7,5,7,32,127);
		glcd_tiny_draw_string(0,0,"Metalloss :");
		glcd_write();

	#endif
	
	
	test_rw_eeprom();
	
	/* Replace with your application code */
	while (1) {
		printf("Hello World!!! %f\r\n",test_float++);
		_delay_ms(500);
	}
}
