/*
 * i2c.c
 *
 * Created: 4/14/2020 11:23:39 AM
 *  Author: Rahadi
 */ 

//#include <io.h>
#include <twi_util.h>
#include "twi_periph.h"
#include <stdint.h>    
#include <stdio.h>    

             
//#define DEBUG_TWI  

#ifndef  F_CPU
#define F_CPU 8000000UL
#endif

#define F_SCL 100000UL // SCL frequency
#define Prescaler 1
#define TWBR_val ((((F_CPU / F_SCL) / Prescaler) - 16 ) / 2)
 
void i2c_periph_init(void)
{
	TWBR = (uint8_t)TWBR_val;
}

uint8_t i2c_periph_start(uint8_t address)
{    
    uint8_t twst;

	// reset TWI control register
	TWCR = 0;
	// transmit START condition
	TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);
	// wait for end of transmission
	while( !(TWCR & (1<<TWINT)) ){
        #ifdef DEBUG_TWI
            printf("START WAIT 1\r\n");
        #endif
    }
       
    #ifdef DEBUG_TWI
        printf("START WAIT 1 DONE\r\n");
    #endif
    	
	// check if the start condition was successfully transmitted
	if((TWSR & 0xF8) != TW_START){ return 1; }
	
	// load slave address into data register
	TWDR = address;
	// start transmission of address
	TWCR = (1<<TWINT) | (1<<TWEN);
	// wait for end of transmission
	while( !(TWCR & (1<<TWINT)) ){
        #ifdef DEBUG_TWI
            printf("START WAIT 2\r\n");
        #endif    
    }
    
    #ifdef DEBUG_TWI
        printf("START WAIT 2 DONE\r\n");
    #endif    
    
	// check if the device has acknowledged the READ / WRITE mode
	twst = TW_STATUS & 0xF8;
	if ( (twst != TW_MT_SLA_ACK) && (twst != TW_MR_SLA_ACK) ) return 1;

    #ifdef DEBUG_TWI
        printf("START I2C DONE\r\n");
    #endif    
	                                            
	return 0;
}

uint8_t i2c_periph_write(uint8_t data)
{
	// load data into data register
	TWDR = data;
	// start transmission of data
	TWCR = (1<<TWINT) | (1<<TWEN);
	// wait for end of transmission
	while( !(TWCR & (1<<TWINT)) ){
        #ifdef DEBUG_TWI
            printf("WRITE WAIT\r\n");
        #endif    
    }
    
	if( (TWSR & 0xF8) != TW_MT_DATA_ACK ){ return 1; }
	
	return 0;
}

//uint8_t i2c_read_ack(void)
uint8_t i2c_read_ack(uint8_t timeout,uint8_t *no_response)
{
	
	// start TWI module and acknowledge data after reception
	TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWEA);
	// wait for end of transmission
	while( !(TWCR & (1<<TWINT)) ){
        #ifdef DEBUG_TWI
            printf("READ ACK WAIT\r\n");
        #endif    
        *no_response=0;
        if(timeout>0)timeout--;
        else{
            *no_response=1;
            break;
         }
        
    }          
	// return received data from TWDR
	return TWDR;
}

uint8_t i2c_read_nack(uint8_t timeout,uint8_t *no_response)
{
	
	// start receiving without acknowledging reception
	TWCR = (1<<TWINT) | (1<<TWEN);
	// wait for end of transmission
	while( !(TWCR & (1<<TWINT)) ){
        #ifdef DEBUG_TWI
            printf("READ NACK WAIT\r\n");
        #endif                        
        *no_response=0;
        if(timeout>0)timeout--;
        else{
            *no_response=1;
            break;
         }
     }
    // return received data from TWDR
	return TWDR;
}

uint8_t i2c_transmit(uint8_t address, uint8_t* data, uint16_t length)
{
    uint16_t i;
	if (i2c_periph_start(address | I2C_WRITE)) return 1;
	               
	for (i = 0; i < length; i++)
	{
		if (i2c_periph_write(data[i])) return 1;
	}
	
	i2c_periph_stop();
	
	return 0;
}

uint8_t i2c_receive(uint8_t address, uint8_t* data, uint16_t length)
{
	uint16_t i;
	
    if (i2c_periph_start(address | I2C_READ)) return 1;
	               
    for (i = 0; i < (length-1); i++)
	{
		data[i] = i2c_read_ack(100,NULL);
	}
	data[(length-1)] = i2c_read_nack(100,NULL);
	
	i2c_periph_stop();
	
	return 0;
}

uint8_t i2c_writeReg(uint8_t devaddr, uint8_t regaddr, uint8_t* data, uint16_t length)
{
	uint16_t i;
	if (i2c_periph_start(devaddr | 0x00)) return 1;

	i2c_periph_write(regaddr);
           
    for (i = 0; i < length; i++)
	{
		if (i2c_periph_write(data[i])) return 1;
	}

	i2c_periph_stop();

	return 0;
}

uint8_t i2c_writeReg16(uint8_t devaddr, uint16_t regaddr, uint8_t* data, uint16_t length)
{
	uint16_t i;
	if (i2c_periph_start(devaddr | 0x00)) return 1;

	i2c_periph_write(regaddr >>8 );
	i2c_periph_write(regaddr & 0xFF);
	            
    for (i = 0; i < length; i++)
	{
		if (i2c_periph_write(data[i])) return 1;
	}

	i2c_periph_stop();

	return 0;
}

uint8_t i2c_readReg(uint8_t devaddr, uint8_t regaddr, uint8_t* data, uint16_t length)
{
	uint16_t i;
	if (i2c_periph_start(devaddr)) return 1;

	i2c_periph_write(regaddr);

	if (i2c_periph_start(devaddr | 0x01)) return 1;
         
    for (i = 0; i < (length-1); i++)
	{
		data[i] = i2c_read_ack(100,NULL);
	}
	data[(length-1)] = i2c_read_nack(100,NULL);

	i2c_periph_stop();

	return 0;
}

uint8_t i2c_readReg16(uint8_t devaddr, uint16_t regaddr, uint8_t* data, uint16_t length)
{
	uint16_t i;
	if (i2c_periph_start(devaddr)) return 1;

	i2c_periph_write(regaddr >>8 );
	i2c_periph_write(regaddr & 0xFF);

	if (i2c_periph_start(devaddr | 0x01)) return 1;
         
    for (i = 0; i < (length-1); i++)
	{
		data[i] = i2c_read_ack(100,NULL);
	}
	data[(length-1)] = i2c_read_nack(100,NULL);

	i2c_periph_stop();

	return 0;
}

void i2c_periph_stop(void)
{
	// transmit STOP condition
	TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWSTO);
}
