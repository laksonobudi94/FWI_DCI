/*
 * _12c.h
 *
 * Created: 4/14/2020 11:24:29 AM
 *  Author: Rahadi
 */ 


#ifndef _I2C_H_
#define _I2C_H_

//tambahkan in twi_periph.h
#include "twi.h"
#include <stdint.h>

#define I2C_READ 0x01
#define I2C_WRITE 0x00

void i2c_periph_init(void);
uint8_t i2c_periph_start(uint8_t address);
uint8_t i2c_periph_write(uint8_t data);

//uint8_t i2c_read_ack(void);
uint8_t i2c_read_ack(uint8_t timeout,uint8_t *no_response);
uint8_t i2c_read_nack(uint8_t timeout,uint8_t *no_response);
uint8_t i2c_transmit(uint8_t address, uint8_t* data, uint16_t length);
uint8_t i2c_receive(uint8_t address, uint8_t* data, uint16_t length);
uint8_t i2c_writeReg(uint8_t devaddr, uint8_t regaddr, uint8_t* data, uint16_t length);
uint8_t i2c_readReg(uint8_t devaddr, uint8_t regaddr, uint8_t* data, uint16_t length);
uint8_t i2c_writeReg16(uint8_t devaddr, uint16_t regaddr, uint8_t* data, uint16_t length);
uint8_t i2c_readReg16(uint8_t devaddr, uint16_t regaddr, uint8_t* data, uint16_t length);
void i2c_periph_stop(void);


#endif /* 12C_H_ */
